<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {  

	function send(){ 


		$nama = $this->input->post('name');
		$company = $this->input->post('company');
		$email = $this->input->post('email');
		$message = $this->input->post('message'); 

		if(empty($nama) || empty($message) || empty($email) || empty($company)){
			echo "Bad Request";
			exit;
		}

		$nama = filter_var($nama, FILTER_SANITIZE_STRING);
		$company = filter_var($company, FILTER_SANITIZE_STRING);
		$email = filter_var($email, FILTER_SANITIZE_STRING);
		$message = filter_var($message, FILTER_SANITIZE_STRING);
		$message = preg_replace("/\n/m", " \n ", $message);

		// Konfigurasi email
        $config = [  
            'protocol'  => 'smtp',
            'smtp_host' => 'mail.lexco-id.com',
            'smtp_user' => 'info@lexco-id.com',  // Email gmail
            'smtp_pass'   => 'Bismillah2021',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465, 
			'send_multipart' => FALSE,  
			'newline'=>"\r\n", 
			'smtp_timeout'=> 20,
        ]; 

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('info@lexco-id.com', 'Lexco Website'); 
        $this->email->to('info@lexco-id.com');   

        // Email Text
        $this->email->subject('Email From Website'); 
        $this->email->message("This Email was send from the website,\n
Name : $nama \n
Company Name: $company \n
Email : $email \n
Message : $message \n");
 
        // Tampilkan pesan sukses atau error
        $msg = $this->email->send();
        if ($msg) {
            echo 'success';
        } else { 
            echo $this->email->print_debugger();
            echo 'false';
        }
   
 
	}
 
}
