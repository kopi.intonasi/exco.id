<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller { 
 
	public function __construct()
    {
            parent::__construct();

    }

	public function data($product)
	{ 
		$prd = [
			'coco-fiber',
			'cocopeat',
			'charcoal-powder', 
			'woodchips',
			'sawdust',
			'green-beans-coffee',
			'coconut-charcoal-briquette',
			'semi-husked-coconut',
			'palm-broom-stick',
			'palm-kernel-expeller',
			'cinnamon',
			'desiccated_coconut'
		];

		if(!in_array($product, $prd)){	 
			show_404();
			exit; 
		}


		$this->load->view('template/header'); 
		$this->load->view('template/navbar'); 		
		$this->load->view('products/'.$product); 
		$this->load->view('content/contactus');
		$this->load->view('template/footer'); 
	}
  

	// public function clean()
	// {
	// 	$this->load->view('template/clean'); 
	// }
 
}
