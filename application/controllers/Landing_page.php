		<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_page extends CI_Controller { 

	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/navbar'); 
		$this->widget(); 
		$this->load->view('template/footer');
	}

 

	function widget(){ 
		$this->load->view('content/slide');
		$this->load->view('content/about');
		$this->load->view('content/feature');
		$this->load->view('content/products');
		$this->load->view('content/gallery');
		$this->load->view('content/team');
		$this->load->view('content/contactus');
	}

	// public function clean()
	// {
	// 	$this->load->view('template/clean'); 
	// }
 
}
