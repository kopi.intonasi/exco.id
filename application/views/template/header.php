<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    
    <!--====== Title ======-->
    <title>Lexco Indonesia</title>
    
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/logo4.png" type="image/png">
        
    <!--====== Magnific Popup CSS ======-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/magnific-popup.css">
        
    <!--====== Slick CSS ======-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/slick.css">
         
        
    <!--====== Bootstrap CSS ======-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    
    <!--====== Default CSS ======-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/default.css">
    
    <!--====== Style CSS ======-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        .section{ 
            padding-top: 40px;
            padding-bottom: 40px;
            position: relative;
            padding-left: 15px;
            padding-right: 15px;
        } 
        .section.bg1{
            background-color: #ffffff; 
        }
        .section.bg2{
            background-color: #e9f2fa; 
        }
        .text-blue{
            color: #0067f4 !important;
        }
        .text-white{
            color: #fff !important;
        }

        button {
            background: none;
            color: inherit;
            border: none;
            padding: 0;
            font: inherit;
            cursor: pointer;
            outline: inherit;
        }
        
        .bottom-icon {
          padding-top: 10px;
          font-size: 20px;
          color: #fff;
          position: fixed;
          right: 40px;
          bottom: 40px; 
          text-align: center;
          z-index: 99;
          -webkit-transition: all 0.3s ease-out 0s;
          -moz-transition: all 0.3s ease-out 0s;
          -ms-transition: all 0.3s ease-out 0s;
          -o-transition: all 0.3s ease-out 0s;
          transition: all 0.3s ease-out 0s;
           }
           
          @media only screen and (max-width: 600px) {
            .bottom-icon{ 
              right: 20px;
              bottom: 20px; 
            }
          }
    </style>
    
    <script src="<?= base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
</head>

<body> 

 <a class="bottom-icon" href="https://api.whatsapp.com/send?phone=6285174247757&text=Hi%2C+I+got+your+WhatsApp+information+from+Lexco+Indonesia+Website.+I+am+interested+in+yout+product.+Can+I+ask+you+some+question+%3F" target="_blank"  title="Whatsapp">
    <img style="width: 60px;" src="<?= base_url(); ?>assets/images/wa.png">
</a>

<!-- 
    <div class="preloader">
        <div class="loader">
            <div class="ytp-spinner">
                <div class="ytp-spinner-container">
                    <div class="ytp-spinner-rotator">
                        <div class="ytp-spinner-left">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                        <div class="ytp-spinner-right">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  -->