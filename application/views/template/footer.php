 
<!-- Footer -->

<section class="footer-area footer-dark">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 text-center">
                <img  style="max-height: 200px" class="img-fluid" src="<?= base_url(); ?>assets/images/logo.png">
            </div>
            <div class="col-lg-6 text-white">
                <b>LEXCO INDONESIA</b> <br>
                SUMATERA BARAT, INDONESIA <br>
                Phone :  <br>
                        <li class="pl-3">+62 851 7424 7757 </li>
                        <li class="pl-3">+62 812 7773 1737 </li>   
                Email : info@lexco-id.com 
                <br>
                <br> 
                <a href="https://www.instagram.com/lexco.id/" target="_blank"  title="Instagram">
                    <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/invert.png"  />
                </a>

                <button type="button"  title="Wechat" data-toggle="modal" data-target="#modalWechat">
                  <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/wechat-invert.png"  />
                </button>
                <!-- <a href="https://linkedin.com" target="_blank"  title="linkedin">
                    <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/linkedin-invert.png"  />
                </a> -->
                <a href="https://api.whatsapp.com/send?phone=6285174247757&text=Hi%2C+I+got+your+WhatsApp+information+from+Lexco+Indonesia+Website.+I+am+interested+in+yout+product.+Can+I+ask+you+some+question+%3F" target="_blank"  title="Whatsapp">
                    <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/wa-invert.png"  />
                </a> 
            </div>
        </div>
    </div>
</section>
<!-- Footer End -->

 <!-- <a href="#" class="bottom-icon"><i class="fa fa-chevron-up"></i></a> --> 



<!-- Modal -->
<div class="modal fade" id="modalWechat" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content"> 
      <div class="modal-body">
        <center>
            <h4 class="pt-2">  <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/wechats.png"  /> Wechat QR </h4> 
             <img  class="img-fluid" src="<?= base_url(); ?>assets/images/qr.png"  />
        </center>
      </div> 
    </div>
  </div>
</div>
 <!--====== Jquery js ======-->
    <script src="<?= base_url(); ?>assets/js/vendor/modernizr-3.7.1.min.js"></script> 
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?= base_url(); ?>assets/js/slick.min.js"></script> 
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script> 
    <script src="<?= base_url(); ?>assets/js/ajax-contact.js"></script> 
    <script src="<?= base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.pkgd.min.js"></script> 
    <script src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/scrolling-nav.js"></script> 
    <script src="<?= base_url(); ?>assets/js/main.js"></script>
    
</body>

</html>