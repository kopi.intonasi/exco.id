<?php 
   $url =  (is_https() ? 'https://' : 'http://')  . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
?>
<section class="navbar-area <?php if($url !== base_url()){echo 'sticky';} ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg">
                   <!-- Logo -->
                    <a class="navbar-brand" href="<?= base_url(); ?>">
                        <?php if($url !== base_url()){ ?> 

                        <img src="<?= base_url(); ?>assets/images/logo3.png" alt="Logo" width="150">

                        <?php }else{ ?> 

                        <img src="<?= base_url(); ?>assets/images/logo2.png" alt="Logo" width="150">

                        <?php } ?>
                    </a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTwo" aria-controls="navbarTwo" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse sub-menu-bar" id="navbarTwo">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#home">Home</a></li>
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#about">About</a></li>
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#feature">Feature</a></li>
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#products">Products</a></li> 
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#gallery">Gallery</a></li>
                            <li class="nav-item"><a class="page-scroll" href="<?= base_url(); ?>#team">Team</a></li> 
                            <li class="nav-item"><a class="page-scroll" href="#contact">Contact Us</a></li> 
                        </ul>
                    </div>
                    
                    <div class="navbar-btn d-none d-sm-inline-block">
                        <ul>
                            <li><a class="solid page-scroll" href="#contact">Get an Offer</a></li>
                        </ul>
                    </div>
                </nav>  
            </div>
        </div>  
    </div> 
</section>

<?php  
   if(base_url() == $url){ ?> 
    <script type="text/javascript">
        $(window).on('scroll', function (event) {
            var scroll = $(window).scrollTop();
            if (scroll < 20) {
                $(".navbar-area").removeClass("sticky");
                $(".navbar-area img").attr("src", "<?= base_url(); ?>assets/images/logo2.png");
            } else {
                $(".navbar-area").addClass("sticky");
                $(".navbar-area img").attr("src", "<?= base_url(); ?>assets/images/logo3.png");
            }
        });
    </script> 
   <?php } ?>