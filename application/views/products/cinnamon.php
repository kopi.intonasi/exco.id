<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/product/c1.png",
	  base_url()."assets/images/product/c2.jpg", 
	  base_url()."assets/images/product/c3.jpg", 
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue"> CINNAMON</h3> 
                  <br />
                  Cinnamon is a spice obtained from the inner bark of several tree species from the genus Cinnamomum. Cinnamon is used mainly as an aromatic condiment and flavouring additive in a wide variety of cuisines, sweet and savoury dishes, breakfast cereals, snack foods, bagels, teas, hot chocolate and traditional foods.
                 </div>
 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div> 


                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 
                  <div class="row">
                  		<div class="col-md-3"> </div>
                  		<div class="col-md-6 card-body table-data" >
                  			<center> <h3 style="color: white; margin-bottom: 20px;"> 
                  				Specification
                  			</h3></center>
                  			<table class="table table-striped table-bordered" style="background-color: white"> 
                  				<tbody>
                  					<tr>
                  						<th> Size </th>
                  						<td> 8-10 cm</td>
                  					</tr>
                  					<tr>
                  						<th> Moisture</th>
                  						<td>13.5% max</td>
                  					</tr>
                  					<tr>
                  						<th> Rolls </th>
                  						<td> 100%</td>
                  					</tr> 
                  					<tr>
                  						<th> Color </th>
                  						<td> Natural </td>
                  					</tr> 
                  					<tr>
                  						<th> Skin </th>
                  						<td> Stem</td>
                  					</tr> 
                  					<tr>
                  						<th> Thickness </th>
                  						<td>1-2 mm</td>
                  					</tr> 
                  					<tr>
                  						<th>Scrape </th>
                  						<td>Clean</td>
                  					</tr> 
                  					<tr>
                  						<th>Mixture </th>
                  						<td>0.50%</td>
                  					</tr> 
                  				</tbody>
                  			</table>
                  		</div>
                  		 
                  		<div class="col-md-3"> </div>
                  </div>
                </div> 
                 
            </div> 
        </div> 
    </section>