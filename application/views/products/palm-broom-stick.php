<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/product/pbs1.jpg",
	  base_url()."assets/images/product/pbs2.jpg",
	  base_url()."assets/images/product/pbs3.png", 
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue"> PALM BROOM STICK</h3> 
                  <br />
                  Palm Sticks Derived From The Midrib Of Oil Palm Trees That Have A Flexible Texture, Palm Sticks Can Be Processed Into Brooms And Become Beautiful Crafts, Such As Plates, Chairs, And Accessories.
                </div>
 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div> 


                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 
                  <div class="row">
                  		<div class="col-md-3"> </div>
                  		<div class="col-md-6 card-body table-data" >
                  			<center> <h3 style="color: white; margin-bottom: 20px;"> 
                  				Specification
                  			</h3></center>
                  			<table class="table table-striped table-bordered" style="background-color: white"> 
                  				<tbody>
                  					<tr>
                  						<th> Length </th>
                  						<td> 40-60 cm </td>
                  					</tr>
                  					<tr>
                  						<th> Flower Leght</th>
                  						<td> 60-90 cm</td>
                  					</tr>
                  					<tr>
                  						<th> Packaging </th>
                  						<td> Gunny Plastic Bag</td>
                  					</tr> 
                  				</tbody>
                  			</table>
                  		</div>
                  		 
                  		<div class="col-md-3"> </div>
                  </div>
                </div> 
                 
            </div> 
        </div> 
    </section>