<style type="text/css">
	.customCardFeature {
		min-height: 400px !important;
	}
</style>
<style type="text/css">
	.imgGallery {
		object-fit: cover;
		width: 50px;
		height: 200px;
	}

	.hover-container:hover .image-popup {
		display: inline-block;
	}
	.table-data {
		background-color: #0067f4;
		border: solid 1px #0067f4;
		border-radius: 10px;
		margin: 10px;
	}
</style>

<?php
	$image = array(
	  base_url()."assets/images/product/kopi-biji-1.png", 
	  base_url()."assets/images/product/kopi-biji-2.png", 
	  base_url()."assets/images/product/kopi-biji-3.png", 
	  base_url()."assets/images/product/kopi-proses-1.png", 
	  base_url()."assets/images/product/kopi-proses-2.png", 
	//   base_url()."assets/images/product/kopi-gudang-1.png", 
	  base_url()."assets/images/product/kopi-gudang-2.png", 
	);

?>
<section id="feature" class="section bg1" style="padding-top: 150 !important">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center mt-4">
				<h3 class="title text-blue">Green Beans Coffee</h3>
				<br />
				Green coffee beans are the raw seeds of coffee cherries that have been
				separated or “processed” and have yet to be roasted. All of a coffee’s
				taste and flavor potential is held within this green seed. This
				potential is ultimately unleashed through roasting the green coffee.
			</div>

			<div class="col-lg-12 col-md-12">
				<div class="row no-gutters grid mt-50">
					<?php foreach ($image as $value) { ?>
					<div class="col-lg-4 col-sm-4 p-1">
						<div class="single-portfolio">
							<div class="portfolio-image">
								<img class="imgGallery" src="<?= $value ?>" alt="" />
								<div
									class="portfolio-overlay d-flex align-items-center justify-content-center"
								>
									<div class="portfolio-content">
										<div class="portfolio-icon">
											<a class="image-popup" href="<?= $value ?>">
												<i class="fa fa-search-plus"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>

            <!-- Products -->

            <?php

                $acehgayo =  array(
                    'nama' => 'Arabica Aceh Gayo',
                    'origin' => 'Gayo Highland, Aceh, Sumatera', 
                    'attitude'=>'1200 MASL', 
                    'temperature'=>'19 - 12 °C',
                    'humidity'=>'12-13 %', 
                    'certificate'=>'FTO, USDA, EU Organic Certificatioh',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'11-26',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'13',
                    'moisture2'=>'13%',
                    'moisture3'=>'13% - 15%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );


                $arabicasolok =  array(
                    'nama' => 'Arabica Solok Radjo',
                    'origin' => 'Solok, West Sumatera', 
                    'attitude'=>'1458 MASL', 
                    'temperature'=>'15 - 17 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'26-44',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'13',
                    'moisture2'=>'13%',
                    'moisture3'=>'13% - 15%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );

                $javapreanger =  array(
                    'nama' => 'Arabica Java Preanger',
                    'origin' => 'Bandung, West Java', 
                    'attitude'=>'1400 - 1600 MASL', 
                    'temperature'=>'15 - 17 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'26-44',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'13',
                    'moisture2'=>'13%',
                    'moisture3'=>'13% - 15%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );

                $arabicasolokselatan =  array(
                    'nama' => 'Arabica Solok Selatan',
                    'origin' => 'South Solok, West Sumatera', 
                    'attitude'=>'479 MASL', 
                    'temperature'=>'19 - 21 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'26-44',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'11',
                    'moisture2'=>'11%',
                    'moisture3'=>'11%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );


                $solokradjo =  array(
                    'nama' => 'Robusta Solok Radjo',
                    'origin' => 'Solok, West Sumatera', 
                    'attitude'=>'1458 MASL', 
                    'temperature'=>'15 - 17 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'-', 
                    'defect2'=>'0-11',
                    'defect3'=>'11-26',
                    'defect4'=>'45-60',
                    'defect5'=>'61-80',
                    'moisture1'=>'-',
                    'moisture2'=>'13%',
                    'moisture3'=>'13% - 15%',
                    'moisture4'=>'13%',
                    'moisture5'=>'13%',
                    'size1'=>'-',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'Medium',
                    'size5'=>'Small',
                );

                $lampung =  array(
                    'nama' => 'Robusta Lampung',
                    'origin' => 'Lampung, Sumatera', 
                    'attitude'=>'900-1400 MASL', 
                    'temperature'=>'15 - 17 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'26-44',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'11%',
                    'moisture2'=>'11%',
                    'moisture3'=>'11%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );



                $robustasolokselatan =  array(
                    'nama' => 'Robusta Solok Selatan',
                    'origin' => 'South Solok, West Sumatera', 
                    'attitude'=>'479 MASL', 
                    'temperature'=>'19 - 21 °C',
                    'humidity'=>'10% - 15%', 
                    'certificate'=>'FTO, USDA, EU Organic Certification',
                    'process'=>'Natural, Semi Wash, Full Wash',
                    'defect1'=>'0', 
                    'defect2'=>'0-11',
                    'defect3'=>'26-44',
                    'defect4'=>'-',
                    'defect5'=>'-',
                    'moisture1'=>'11%',
                    'moisture2'=>'11%',
                    'moisture3'=>'11%',
                    'moisture4'=>'-',
                    'moisture5'=>'-',
                    'size1'=>'Medium',
                    'size2'=>'Medium',
                    'size3'=>'Medium',
                    'size4'=>'-',
                    'size5'=>'-',
                );

                $arr = array($acehgayo,$arabicasolok,$javapreanger, $arabicasolokselatan, $solokradjo,$lampung,$robustasolokselatan );

            ?>

			<style>
				.location {
					font-size: 18;
					font-weight: bold;
				}

				.subs {
					font-size: 14;
				}
			</style>

            <div class="w-100 pt-5"> 
				<h3 class="title text-blue text-center">Products</h3>
            </div>
            
            <?php  foreach ($arr as $key => $value) { ?>
			<div
				class="col-lg-12 text-center mb-4 mt-4"
				style="box-shadow: 0 3px 10px rgb(0 0 0 / 0.1)"
			>
				<div class="row">
					<div class="col-md-3 p-3" style="background-color: #fff9e8">
						<img
							style="border-radius: 25px; max-width: 75%"
							class="img-fluid"
							src="<?= base_url().'assets/images/gallery/example.png'; ?>"
						/>
						<center class="mt-4">
							<div class="location"><?= $value['nama'] ?></div>
							<div class="subs"><?= $value['origin'] ?></div>
						</center>
					</div>

					<div class="col p-3" style="background-color: white">
						<div class="table-responsive">
							<table class="table table-striped">
								<tr style="background-color: #0166f3; color: white">
									<th>Attitude</th>
									<th>Temperature</th>
									<th>Humidity</th>
									<th>Certificate</th>
									<th>Process</th>
								</tr>

								<tr>
									<td><?= $value['attitude'] ?></td>
									<td><?= $value['temperature'] ?></td>
									<td><?= $value['humidity'] ?></td>
									<td><?= $value['certificate'] ?></td>
									<td><?= $value['process'] ?></td>
								</tr>
							</table>

							<table class="table table-striped">
								<tr style="background-color: #ffe598; color: black">
									<th>Specification</th>
									<th>Grade Specialty</th>
									<th>Grade 1</th>
									<th>Grade 3</th>
									<th>Grade 4A</th>
									<th>Grade 4B</th>
								</tr>

								<tr>
									<td>Defect</td>
									<td>0</td>
									<td>0-11</td>
									<td>11-26</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td>Moisture</td>
									<td>13%</td>
									<td>13%</td>
									<td>13% - 15%</td>
									<th>-</th>
									<th>-</th>
								</tr>
								<tr>
									<td>Size</td>
									<td>Medium</td>
									<td>Medium</td>
									<td>Medium</td>
									<th>-</th>
									<th>-</th>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

            <?php } ?>
		</div>
	</div>
</section>
