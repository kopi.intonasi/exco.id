<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/product/c1.png",
	  base_url()."assets/images/product/c2.jpg", 
	  base_url()."assets/images/product/c3.jpg", 
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue"> DESICCATED COCONUT</h3> 
                  <br />
                  Desiccated Coconut is consistenly produced and handled under condition meeting Codex General principles of Food Hygiene, EU & FDA and comply with all current national and international food legislation and applicable regulatory codes.											
                </div>

                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 


					
					<h3 class="title text-blue">Grade</h3 >
						<br />

					<div class="container">
						<div class="d-flex justify-content-center">
						<ul class="nav nav-tabs">
						  <li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#grade-a">
								HIGH FAT DESICCATED COCONUT MEDIUM GRADE
							</a>
						  </li>
						  <li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#grade-b">
								HIGH FAT DESICCATED COCONUT FINE GRADE
							</a>
						  </li>
						</ul>

					</div>
					  
						<div class="tab-content">
						  <div class="tab-pane active" id="grade-a">
                <?php $this->load->view('products/dc_medium_grade.php'); ?>
						  </div>

						  <div class="tab-pane" id="grade-b">
							<?php $this->load->view('products/dc_fine_grade.php'); ?>
						  </div>
						</div>
					  </div>
					  
 
                 
            </div> 
        </div> 
    </section>