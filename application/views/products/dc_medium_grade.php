<!-- Content for Grade A goes here -->

<div class="row pt-3 pb-2">
	<div class="col-md-12">
		<img
			src='<?= base_url()."assets/images/product/Desiccated Coconut Medium Grade.png" ?>'
			class="img-fluid rounded"
			style="max-width: 20%"
		/>
		<br />
		<br />
		Desiccated Coconut is consistenly produced and handled under condition
		meeting Codex General principles of Food Hygiene, EU & FDA and comply with
		all current national and international food legislation and applicable
		regulatory codes.

		<br />
	</div>
</div>

<br />
<div class="text-left"><h4 class="text-blue">A. Physical</h4></div>
<br />
<table class="table table-bordered table-striped">
	<tr>
		<th>Appereance & Colour</th>
		<td colspan="2">
			White, reasonably free from yellow specs and other discolorations.
		</td>
	</tr>
	<tr>
		<th>Flavour</th>
		<td colspan="2">
			Mild and sweet characteristic, free foreign flavour and odours.
		</td>
	</tr>
	<tr>
		<th>Size</th>
		<td colspan="2">Medium granules of coconut.</td>
	</tr>
	<tr>
		<th>Organic Impurities</th>
		<td colspan="2">Not more than 4 pcs dark spec’s per 100 gr sample</td>
	</tr>
	<tr>
		<th>Foreign Material</th>
		<td colspan="2">Absent</td>
	</tr>
	<tr>
		<th rowspan="8">Particle Size</th>
		<td>No. of Sieves (ASTM)</td>
		<td>% Retained On</td>
	</tr>
	<tr>
		<td>Mesh 6 (3.35 mm)</td>
		<td>0 — 6</td>
	</tr>
	<tr>
		<td>Mesh 10 (2.00 mm)</td>
		<td>40 — 80</td>
	</tr>
	<tr>
		<td>Mesh 12 (1.70 mm)</td>
		<td>18 — 32</td>
	</tr>
	<tr>
		<td>Mesh 14 (0.40 mm)</td>
		<td>0 — 15</td>
	</tr>
	<tr>
		<td>Mesh 16 (1.18 mm)</td>
		<td>0 — 10</td>
	</tr>
	<tr>
		<td>Mesh 20 (0.85 mm)</td>
		<td>0 — 8</td>
	</tr>
	<tr>
		<td>Pan</td>
		<td>0 — 8</td>
	</tr>
</table>

<!-- B -->

<br />
<div class="text-left"><h4 class="text-blue">B. Chemical</h4></div>
<br />
<table class="table table-bordered table-striped">
	<tr>
		<th>Parameter</th>
		<th>Limit</th>
		<th>Test Method</th>
	</tr>
	<tr>
		<th>Moisture Content (%)</th>
		<td>3 % maximum</td>
		<td>Oven Method</td>
	</tr>
	<tr>
		<th>Total Fat (%)</th>
		<td>65 + 5</td>
		<td>SNI 01 2891-1992 butir 8.1</td>
	</tr>
	<tr>
		<th>FFA (as Lauric Acid) (%)</th>
		<td>0.10 maximum</td>
		<td>AOCS Ca 5a-40, 2012</td>
	</tr>
	<tr>
		<th>Residual SO2 (ppm)</th>
		<td>Not detected (<5) or 50 ppm max. Upon request</td>
		<td>Tunner Method (1963)</td>
	</tr>
	<tr>
		<th>Residual Chlorine</th>
		<td>Not Detected (<0.02)</td>
		<td>8021 — HACH spectrophotometer</td>
	</tr>
	<tr>
		<th>pH</th>
		<td>6.0 to 6.7</td>
		<td>AOAC 943.02, 19t" Edition 2012</td>
	</tr>
	<tr>
		<th>Invert Sugar</th>
		<td>0.60 maximum</td>
		<td>Refractometer</td>
	</tr>
	<tr>
		<th>Lipase activity (U/g)</th>
		<td>Not detected (<5)</td>
		<td>ELISA</td>
	</tr>
	<tr>
		<th>Total aflatoxin</th>
		<td>Negative</td>
		<td>Diagnostic Kit</td>
	</tr>
	<tr>
		<th>Organoplosphorus</th>
		<td>Negative</td>
		<td>GC Pesticide Residues</td>
	</tr>
	<tr>
		<th>Heavy Metal (ppm)</th>
		<td>-</td>
		<td>-</td>
	</tr>
</table>

<!-- C -->
<br />
<div class="text-left"><h4 class="text-blue">C. MICROBIOLOGICAL</h4></div>
<br />
<table class="table table-bordered table-striped">
	<tr>
		<th>Parameter</th>
		<th>Limit</th>
		<th>Test Method</th>
	</tr>

	<tr>
		<th>Total Plate Count</th>
		<td>5,000 cfu/gram max.</td>
		<td>BAM Chapter 3 Edition 2001</td>
	</tr>
	<tr>
		<th>Enterobacteriaceae</th>
		<td>100 cfu/gram max</td>
		<td>ISO 21528-2:2017</td>
	</tr>
	<tr>
		<th>Yeast</th>
		<td>50 cfu/gram max.</td>
		<td>BAM Chapter 18, 2001</td>
	</tr>
	<tr>
		<th>Moulds</th>
		<td>50 cfu/gram max.</td>
		<td>BAM Chapter 18, 2001</td>
	</tr>
	<tr>
		<th>E. Coli in 10g</th>
		<td>Negative</td>
		<td>BAM Chapter 4, 2002</td>
	</tr>
	<tr>
		<th>Salmonella in 2Sg</th>
		<td>Negative</td>
		<td>ISO 6579-1, 2017</td>
	</tr>
</table>
