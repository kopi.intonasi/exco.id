<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/product/shc1.jpeg",
	  base_url()."assets/images/product/shc2.jpg",
	  base_url()."assets/images/product/shc3.png", 
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue"> SEMI HUSKED COCONUT </h3> 
                  <br />
                  Semi Husked Coconut is raw coconut which the fibers have been peeled off. Coconut has various contents that are beneficial to human body, such as fat, protein, minerals and B vitamins. Coconut is high in manganese content that is essential for the health of human bone. It also increase metabolism of carbohydrates, protein and cholesterol. In addition, coconut is also rich in copper and iron which help to form red blood cells.
                </div>
 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div> 


                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 
                  <div class="row">
                  		<div class="col-md-3"> </div>
                  		<div class="col-md-6 card-body table-data" >
                  			<center> <h3 style="color: white; margin-bottom: 20px;"> 
                  				Specification
                  			</h3></center>
                  			<table class="table table-striped table-bordered" style="background-color: white"> 
                  				<tbody>
                  					<tr>
                  						<th> Style </th>
                  						<td> Fresh </td>
                  					</tr>
                  					<tr>
                  						<th> Product Type</th>
                  						<td> Tropical & Sub Tropical Fruits</td>
                  					</tr>
                  					<tr>
                  						<th> Variety </th>
                  						<td> Matured Coconut</td>
                  					</tr>
                  					<tr>
                  						<th> Cultivation Type </th>
                  						<td> Common </td>
                  					</tr>
                  					<tr>
                  						<th> Colour </th>
                  						<td> Dark Brown</td>
                  					</tr>
                  					<tr>
                  						<th rowspan=3> Weight </th>
                  						<td> Grade A ( > 1Kg ) </td>
                  					</tr>
                  					<tr> 
                  						<td> Grade B ( 0.8-1Kg )</td>
                  					</tr>
                  					<tr> 
                  						<td> Grade C (< 0.8 Kg) </td>
                  					</tr>
                  					<tr>
                  						<th rowspan="2"> Load Ability  </th>
                  						<td> D22-24 MT / Container 40" </td>
                  					</tr>
                  					<tr> 
                  						<td> 11 MT / Container 20"</td>
                  					</tr>
                  				</tbody>
                  			</table>
                  		</div>
                  		 
                  		<div class="col-md-3"> </div>
                  </div>
                </div> 
                 
            </div> 
        </div> 
    </section>