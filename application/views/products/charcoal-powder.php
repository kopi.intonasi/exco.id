<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/product/Arang1.png",
	  base_url()."assets/images/product/Arang2.png",
	  base_url()."assets/images/product/Arang3.png",
	  base_url()."assets/images/product/Arang4.png",
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue">  COCONUT CHARCOAL CHIP & POWDER </h3> 
                  <br />
                  Charcoal powder or charcoal powder is one of the natural ingredients to give black color to various cakes and other food products, such as noodles, ice cream, and others. 
                </div>
 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div> 


                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 
                  <div class="row">
                  		<div class="col-md-1"> </div>
                  		<div class="col-md-10 card-body table-data" >
                  			<center> <h3 style="color: white; margin-bottom: 20px;"> 
                  				Detail Data
                  			</h3></center>
                  			<table class="table table-hover table-striped table-bordered" style="background-color: white"> 
                  				<thead>
                  					<tr class="text-center">
                  						<th rowspan="2" style="vertical-align: middle;"> Parameter </th>
                  						<th rowspan="2" style="vertical-align: middle;"> Unit </th>
                  						<th colspan="2" style="vertical-align: middle;"> Result</th> 
                  						<th rowspan="2" style="vertical-align: middle;"> Method </th> 
                  					</tr>
                  					<tr class="text-center">  
                  						<th> Wet Basic </th>
                  						<th> Dry Basic </th> 
                  					</tr>
                  				</thead>
                  				<tbody> 
                  					<tr>
                  						<td> Moisture</td>
                  						<td> Pct </td>
                  						<td> 7.21 </td>
                  						<td> - </td>
                  						<td> PcASTM D1762-84 (REAPPROVED 2021) </td>
                  					</tr>
                  					<tr>
                  						<td> Ash Content </td>
                  						<td> Pct </td>
                  						<td> 1.16 </td>
                  						<td> 1.25 </td>
                  						<td> ASTM D1762-84 (REAPPROVED 2021) </td>
                  					</tr>
                  					<tr>
                  						<td> Volatile Matter </td>
                  						<td> Pct </td>
                  						<td> 11.98 </td>
                  						<td> 12.91 </td>
                  						<td> ASTM D1762-84 (REAPPROVED 2021) </td>
                  					</tr>
                  					<tr>
                  						<td> Fixed Carbon </td>
                  						<td> Pct </td>
                  						<td> 79.65 </td>
                  						<td> 85.84 </td>
                  						<td> By Calculation </td>
                  					</tr>
                  				</tbody>
                  			</table>
                  		</div>
                  		 
                  		<div class="col-md-1"> </div>
                  </div>
                </div> 
                 
            </div> 
        </div> 
    </section>