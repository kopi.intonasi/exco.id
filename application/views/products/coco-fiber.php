<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
    .table-data{
    	background-color: #0067f4;
    	border: solid 1px #0067f4;
    	border-radius: 10px; 
    	margin: 10px;
    }
 </style>

<?php
	$image = array(
	  base_url()."assets/images/gallery/gallery7.jpg",
	  base_url()."assets/images/gallery/gallery3.jpg",
	  base_url()."assets/images/gallery/gallery2.jpg",
	  base_url()."assets/images/gallery/gallery1.jpg",
	  base_url()."assets/images/gallery/gallery2.png",
	  base_url()."assets/images/gallery/gallery5.jpg", 
	);

?>
    <section id="feature" class="section bg1 " style="padding-top: 150 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center   mt-4">
                  <h3 class="title text-blue">  Coconut Fiber</h3> 
                  <br />
                  Coconut Fiber is a natural fiber from husk of coconut which is obtained from the machining process (extraction) and is used as the basic material for making products such as doormats, mattresses, ropes, etc.Coir is the fibrous part of the coconut shell that is found between the hard shell and the outside of the coconut.  
                </div>
 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div> 


                <div class="col-lg-12 text-center mb-4 mt-4 pt-4 "> 
                  <div class="row">
                  		<div class="col-md-3"> </div>
                  		<div class="col-md-6 card-body table-data" >
                  			<center> <h3 style="color: white; margin-bottom: 20px;"> 
                  				Specifications
                  			</h3></center>
                  			<table class="table table-hover table-striped table-bordered" style="background-color: white">  
                  				<tbody>
                            <tr>
                              <th> Dimension</th>
                              <td> 115x85x40 cm </td>
                            </tr> 
                            <tr>
                              <th> Weight</th>
                              <td> 110-150 kg/Bale </td>
                            </tr> 
                            <tr>
                              <th> Water Content</th>
                              <td> 10-15% </td>
                            </tr> 
                            <tr>
                              <th> Fiber</th>
                              <td> 10-20 cm </td>
                            </tr> 
                            <tr>
                              <th> Color</th>
                              <td> Golden Brown </td>
                            </tr> 
                            <tr>
                              <th> Dust</th>
                              <td> 5% to 10% </td>
                            </tr> 
                            <tr>
                              <th> Max moisture content</th>
                              <td> 10% Max </td>
                            </tr> 
                            <tr>
                              <th> Packing in Bale</th>
                              <td> With roped by plastic band </td>
                            </tr> 
                  				</tbody>
                  			</table>
                  		</div>
                  		 
                  		<div class="col-md-3"> </div>
                  </div>
                </div> 
                 
            </div> 
        </div> 
    </section>