
<?php

$image = array(
  base_url()."assets/images/gallery/gallery7.jpg",
  base_url()."assets/images/gallery/gallery3.jpg",
  base_url()."assets/images/gallery/gallery2.jpg",
  base_url()."assets/images/gallery/gallery1.jpg",
  base_url()."assets/images/gallery/gallery2.png",
  base_url()."assets/images/gallery/gallery5.jpg",
  base_url()."assets/images/gallery/gallery6.jpg",
  base_url()."assets/images/gallery/gallery4.jpg",
  base_url()."assets/images/gallery/gallery1.jpg",
);

?>
 <style type="text/css">
   .imgGallery {
     object-fit: cover;
     width: 50px;
     height: 200px;
    } 

    .hover-container:hover .image-popup {
       display:inline-block;
    }
 </style>
    <section id="gallery" class="section bg2 ">
        <div class="container">
            <div class="row justify-content-center"> 
                <div class="col-lg-12 text-center  mt-4">
                  <h3 class="title text-blue">  Gallery </h3> 
                </div>
            </div>  
            <div class="row"> 
                <div class="col-lg-12 col-md-12">
                    <div class="row no-gutters grid mt-50">

                      <?php foreach ($image as $value) { ?>
                          <div class="col-lg-4 col-sm-4 p-1  ">
                            <div class="single-portfolio">
                                <div class="portfolio-image ">
                                    <img class="imgGallery" src="<?= $value ?>" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div  class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup" href="<?= $value ?>">
                                                  <i class="fa fa-search-plus "></i>
                                                </a> 
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                      <?php } ?> 
                    </div>  
                </div>
                <!-- <div class="col-md-12 text-center p-4"> 
                  <button class="btn btn-lg btn-primary p-3 pl-4 pr-4 ">
                    <i class="fa fa-image fa-fw"></i> See All Images
                  </button>
                </div> -->
            </div>  
        </div>  
    </section>