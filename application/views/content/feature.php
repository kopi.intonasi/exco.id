<style type="text/css">
  .customCardFeature{
    min-height: 400px !important;
  }  
</style>

    <section id="feature" class="section bg2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center mb-4 mt-4">
                  <h3 class="title text-blue">  Our Key Feature</h3> 
                </div>
                <div class="col-lg-4 p-2"> 
                  <div class="card customCardFeature">
                    <div class="card-body text-center" >  
                       <img class="img-fluid" style="max-height: 100px;" src="<?= base_url(); ?>assets/images/icon/love.png">
                       <h4 class="pt-3 pb-3"> Integrity </h4>
                       Integrity signifies an honesty in telling the truth, being responsible for all actions, and knowing the applicable law or code of ethics. This is the integrity we hold to achieve our corporate goals.

                    </div>
                  </div>
                </div>
                <div class="col-lg-4 p-2">
                   <div class="card customCardFeature">
                    <div class="card-body text-center" >  
                       <img class="img-fluid" style="max-height: 100px;" src="<?= base_url(); ?>assets/images/icon/shakehand.png">
                       <h4 class="pt-3 pb-3"> Commitment </h4>
                       Our commitment in doing business is to always do what we say, keep our promises, and serve our customers wholeheartedly with fairness and respect.

                    </div>
                  </div>
                </div> 
                <div class="col-lg-4 p-2">  
                   <div class="card customCardFeature">
                    <div class="card-body text-center" >  
                       <img class="img-fluid" style="max-height: 100px;" src="<?= base_url(); ?>assets/images/icon/quality2.png">
                       <h4 class="pt-3 pb-3"> Quality </h4>
                       Quality is one of the most valuable assets, because with quality we can meet the needs and increase customer satisfaction. The important thing to note is to always ensure the specifications are in accordance with customer requests.

                    </div>
                  </div>
                </div> 
            </div> 
        </div> 
    </section>