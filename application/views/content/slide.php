<!-- Content For Slide -->
<style type="text/css">

    .img1 {
      background: url(assets/images/1.jpg);
    }

    .img2 {
      /*background: url(assets/images/2_2.jpg); */
      background: linear-gradient(rgba(0, 0, 0, .3), rgba(0, 0, 0, .3)),  url(assets/images/2_2.jpg); 

    }
    .carousel-item{
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      height: 100vh; 
    }
</style>
 
<section id="home" class="slider_area">
        <div id="dimCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#dimCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#dimCarousel" data-slide-to="1"></li> 
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item img1 active">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="slider-content">
                                    <h1 class="title">Lexco Indonesia</h1>
                                    <p class="text">We Provide you various kinds of Product <br>and We are ready to supply to your country
                                    </p>


                                    <p class="text"></p>
                                    <ul class="slider-btn rounded-buttons">
                                        <li><a class="main-btn rounded-one page-scroll" href="#about">About Us</a></li>
                                        <li><a class="main-btn rounded-two page-scroll" href="#products">See Products</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                    </div>  
                </div>  


                <div class="carousel-item img2">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content">
                                    <h1 class="title" style="margin-top:100px;">Best Choice of partner
</h1> 
                                     
                                </div>  
                            </div>
                        </div>  
                    </div>  

                </div> 
            </div>

            <a class="carousel-control-prev" href="#dimCarousel" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#dimCarousel" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </section>