 

    <section id="team" class="section bg1">
        <div class="container">
            <div class="row">
              

                <div class="col-lg-12 text-center mb-4 mt-4">
                  <h3 class="title text-blue"> Our Team </h3> 
                </div> 

                <div class="col-md-1"> </div>
                
                <div class="col-lg-2 p-2 text-center">   
                  <div class="p-4"> 
                       <img class="img-fluid rounded-circle"  src="<?= base_url(); ?>assets/images/tim/hanif.png"> 
                  </div>
                    <div class="text-blue" style="font-size: 18pt; padding-bottom: 20px;">  M. HANIF </div> 
                    <div style="font-size: 10pt; font-weight: bold;">  Chief Executive Officer <br> (CEO) </div>  
                       <br> 
                        <a href="https://www.linkedin.com/in/muhammad-hanif-045444178/" target="_blank" title="Linkedin" >
                         <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/linkedin.png"  />
                       </a>
                       <a href="https://www.instagram.com/haniefmuhammad_/" target="_blank"  title="Instagram">
                        <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
                       </a>
                </div> 
                
                <div class="col-lg-2 p-2 text-center">   
                  <div class="p-4">
                       <img class="img-fluid rounded-circle"  src="<?= base_url(); ?>assets/images/tim/dzaky.png"> 
                  </div>
                    <div class="text-blue" style="font-size: 14pt"> AHMAD DZAKY ZAIN </div> 
                    <div style="font-size: 10pt; font-weight: bold;"> Chief Operating Officer <br> (COO) </div>  
                       <!-- <i>Posisi</i>  -->
                       <br>  
                    <a href="https://instagram.com/dzakyzain/" target="_blank"  title="Instagram">
                        <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
                       </a>
                </div>  
                <div class="col-lg-2 p-2 text-center">   
                  <div class="p-4">
                       <img class="img-fluid rounded-circle"  src="<?= base_url(); ?>assets/images/tim/dhani.png"> 
                  </div>
                    <div class="text-blue" style="font-size: 14pt">  DHANI WAHYU EKAPUTRA </div> 
                    <div style="font-size: 10pt; font-weight: bold;">  Chief Financial Officer <br> (CFO) </div>  
                       <!-- <i>Posisi</i>  -->
                       <br> 
                        <a href="https://www.linkedin.com/in/dhani-wahyu-ekaputra-707b03197" target="_blank" title="Linkedin" >
                         <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/linkedin.png"  />
                       </a>
                       <a href="https://instagram.com/dhniwhyu/" target="_blank"  title="Instagram">
                        <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
                       </a>
                </div> 
                <div class="col-lg-2 p-2 text-center">   
                  <div class="p-4">
                       <img class="img-fluid rounded-circle"  src="<?= base_url(); ?>assets/images/tim/dzaki2.png"> 
                  </div>
                    <div class="text-blue" style="font-size: 14pt">  MUHAMMAD DZAKI AULIA </div> 
                    <div style="font-size: 10pt; font-weight: bold;">  Chief Marketing Officer <br> (CMO) </div>  
                    <br />
 
                        <a href="https://www.linkedin.com/in/muhammad-dzaki-aulia-0ba3b2211/" target="_blank" title="Linkedin" >
                         <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/linkedin.png"  />
                       </a>
                       <a href="https://www.instagram.com/muhammaddzakiiii/?hl=id" target="_blank"  title="Instagram">
                        <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
                       </a>
                </div>
                <div class="col-lg-2 p-2 text-center">   
                  <div class="p-4">
                       <img class="img-fluid rounded-circle"  src="<?= base_url(); ?>assets/images/tim/zikri.png"> 
                  </div>
                    <div class="text-blue" style="font-size: 14pt;  padding-bottom: 28px;"> ZIKRI HAIKAL F. </div> 
                    <div style="font-size: 10pt; font-weight: bold;">  Assistant Chief Marketing Officer </div>  
                    <br />
 
                        <a href="https://www.linkedin.com/in/zikri-fadli-a71748218" target="_blank" title="Linkedin" >
                         <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/linkedin.png"  />
                       </a>
                       <a href="https://www.instagram.com/5yold/" target="_blank"  title="Instagram">
                        <img style="width: 40px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
                       </a>
                </div>
                <div class="col-md-1"> </div>

                

            </div> 
        </div> 
    </section>