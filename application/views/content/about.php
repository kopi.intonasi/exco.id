<style type="text/css">
	.aboutUs{
		font-size: 20;
	}	
</style>

    <section id="about" class="about-area section bg1">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="faq-content mt-45 text-center">
                        <div class="about-title"> 
                            <h4 class="title text-blue"> About Us</h4>
                        </div> <!-- faq title -->
                        <br />

                        <div class="about-accordion">
                            <div class="accordion" >
                                <div class="card aboutUs">
                                   	Lexco is professional and competitive export company located in Indonesia. Our Value is customer-first with high integrity and better serve. We believe our value to deliver the satisfaction. 

                                   	<br>
                                   	<br>
                                   	We provide global needs, especially specialty coffee, coconut & palm derivative product and spices, etc. In order to fulfill the demand, our company is strongly dedicated for the best quality and services.

                                   	<!-- <br>
                                   	<br>
                                   	We have a strategy for success by developing a network of partners across the companies we manage. We always ensure readiness in our processing plants, to be able to produce the highest quality products at prices that can compete in the international market.
                                   	<br>
                                   	<br>
									Our priority is to fulfill our customers demands and satisfaction needs in accordance with our customers' expectations. -->


  
                                </div>  
                            </div>
                        </div>  
                    </div>  
                </div>
                <div class="col-lg-4">
                    <div class="about-image mt-50">
                        <img src="<?= base_url(); ?>assets/images/coconut1.jpg" alt="about">
                    </div> 
                </div>
            </div> 
        </div> 
    </section>