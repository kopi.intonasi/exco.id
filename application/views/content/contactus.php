
<style type="text/css"> 
        .emailUs{
            background-color:  #0067f4 !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            padding: 30px; 
        }
        .contactUs{
            background-color:  #fff !important;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            padding: 30px; 
        }
        #frmz label{
        	color: #fff;
        }
        .required::before{
 		 content: "* ";
 		 color: #ffdd00;
        }
        .sendButton{
        	background-color: #fff;
        	border-color: #fff;
        	color: #0067f4;
        }
</style>
    <section id="contact" class="section bg2">
        <div class="container">
            <div class="row p-4">
            	<div class="col-md-6 contactUs">
            		<div class="text-center"> 
	            		<a target="_blank" href="https://www.freepik.com/free-vector/flat-design-illustration-customer-support_12982910.htm">
	            		<img class="img-fluid" src="<?= base_url(); ?>assets/images/contact.png" style="max-height: 250px;">
	            		</a> 

                  		<h3 class="title text-blue pt-3 pb-3">  Contact Us </h3> 
            		</div>
            		<div style="font-size: 20px">
            			<b>LEXCO INDONESIA</b> <br>
						SUMATERA BARAT, INDONESIA <br>
						Phone : 
						<li class="pl-3">+62 851 7424 7757 </li>
						<li class="pl-3">+62 812 7773 1737 </li>  
						Email : info@lexco-id.com 
						<br>
						<br>
						<div class="text-center">
						<a href="https://www.instagram.com/lexco.id/" target="_blank"  title="Instagram">
							<img style="width: 60px;" src="<?= base_url(); ?>assets/images/icon/ig.png"  />
						</a> 
						<button type="button"  title="Wechat" data-toggle="modal" data-target="#modalWechat">
						  <img style="width: 60px;" src="<?= base_url(); ?>assets/images/icon/wechats.png"  />
						</button>
						<!-- <a href="https://linkedin.com" target="_blank"  title="linkedin">
							<img style="width: 60px;" src="<?= base_url(); ?>assets/images/icon/linkedin.png"  />
						</a> -->
						<a href="https://api.whatsapp.com/send?phone=6285174247757&text=Hi%2C+I+got+your+WhatsApp+information+from+Lexco+Indonesia+Website.+I+am+interested+in+yout+product.+Can+I+ask+you+some+question+%3F" target="_blank"  title="Whatsapp">
							<img style="width: 60px;" src="<?= base_url(); ?>assets/images/icon/wa square.png"  />
						</a>
						</div>
            		</div>
            	</div> 

            	<div class="col-md-6 emailUs "> 
                  	<h3 class="title text-white pt-3 pb-3 text-center"> 
                  		What we can do for you ?
                  	 </h3> 
                  	 <br>
                  	 <form id="frmz">
                  	 	<div class="form-group pb-3">
						    <label class="required">Name</label>
						    <input name="name"  type="text" class="form-control"  required="">
						 </div>
                  	 	<div class="form-group pb-3">
						    <label class="required">Company</label>
						    <input name="company"  type="text" class="form-control" required="">
						 </div>
                  	 	<div class="form-group pb-3">
						    <label class="required">Email address</label>
						    <input name="email"  type="email" class="form-control"   required="">
						 </div>

                  	 	<div class="form-group pb-3">
						    <label class="required">Your Message</label>
						    <textarea name="message" class="form-control" required=""></textarea>
						 </div>
						 <div id="alertFailed" class="alert alert-danger" style="display: none;">
						 	<i class="fa fa-close"> </i>
						 	Failed to send email, please try again
						 </div>
						 <div class="p-3 text-center">
						 	<button id="btnSend" type="submit" class="btn sendButton">
						 		<i class="fa fa-send"></i> Send 
						 	</button>
						 </div>
                  	 </form>
                  	 <div id="alertDiv" class="text-center" style="display: none;">
                  	 	<img src="<?= base_url(); ?>assets/images/icon/check.png" style="max-height: 100px">
                  	 	<h3 class="text-white mb-2 mt-4"> Thank you for Emailing Us </h3>
                  	 	<h5 class="text-white"> We will reply immediately </h5>
                  	 </div>
            	</div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    	var btnSendDefault = '<i class="fa fa-send"></i> Send ';
    	var btnSendLoading = '<i class="fa fa-spinner  fa-spin"></i> Please Wait ';

    	$("#frmz").submit(function(event){

		 // Prevent default posting of form - put here to work in case of errors
		 event.preventDefault();
		 var values = $(this).serialize();

		 $.ajax({
		        url: "<?= base_url(); ?>email/send",
		        type: "post",
		        data: values ,
		        beforeSend: function(){
			        $('#btnSend').attr('disabled','disabled');
			        $('#btnSend').html(btnSendLoading);
				    $('#alertFailed').fadeOut(); 
			    },
		        success: function (response) { 
			        $('#btnSend').removeAttr('disabled');
			        $('#btnSend').html(btnSendDefault);
 
		        	if(response=="success"){
				        $('#frmz').fadeOut(400, function(){
						    $('#alertDiv').fadeIn();  
						});   
		        	}else{  
				        $('#alertFailed').fadeIn(); 
		          		 console.log("err");
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
			        $('#btnSend').removeAttr('disabled');
			        $('#btnSend').html(btnSendDefault);
			        $('#alertFailed').fadeIn(); 
		           console.log("err");
		        }
		    });
		});
    </script>