
  <style type="text/css">
    .btn.btn-custom {
      background-color: #105e00 !important;
      border-color: #105e00 !important;
      color: white;
    }
    .card.card-custom{
      border: 2px solid hsl(110, 100%, 18%) !important;
      /* min-height: 600px; */
    }
    .category-custom{
      color: #105e00 !important;;
    }


    .btn.btn-custom2 {
      background-color: #5c2c00 !important;
      border-color: #5c2c00 !important;
      color: white;
    }
    .card.card-custom2{
      border: 2px solid #5c2c00 !important;
      min-height: 600px;
    }
    .category-custom2{
      color: #5c2c00 !important;;
    }
    .div-link{
      cursor: pointer;
    }
    .div-link:hover{
      background-color: #105e00;
      color: white;
    }

    .div-link:hover .card{
      background-color: #105e00;
      color: white;
    }
    .div-link:hover .title{ 
      color: white;
    }

  </style>

  <?php 

    $data_product = array(
      array('product'=>'Coconut Fiber', 'link'=>'product/data/coco-fiber', 'photo'=>'assets/images/product/cocofiber.png'), 
      array('product'=>'Green Beans Coffee', 'link'=>'product/data/green-beans-coffee', 'photo'=>'assets/images/product/kopi-biji-1.png'),

      array('product'=>'Desiccated Coconut', 'link'=>'product/data/desiccated_coconut', 'photo'=>'assets/images/product/Desiccated Coconut Fine Grade.png'),
      array('product'=>'Charcoal Chip & Powder', 'link'=>'product/data/charcoal-powder', 'photo'=>'assets/images/product/charcoal.png'),
      array('product'=>'Charcoal Briquette', 'link'=>'product/data/coconut-charcoal-briquette', 'photo'=>'assets/images/product/Bricquette2.png'),
      array('product'=>'Semi Husked Coconut', 'link'=>'product/data/semi-husked-coconut', 'photo'=>'assets/images/product/shc3.png'),
      array('product'=>'Palm Broom Stick', 'link'=>'product/data/palm-broom-stick', 'photo'=>'assets/images/product/pbs1.jpg'),
      array('product'=>'Palm Kernel Expeller', 'link'=>'product/data/palm-kernel-expeller', 'photo'=>'assets/images/product/pke2.jpg'),
      array('product'=>'Cinnamon', 'link'=>'product/data/cinnamon', 'photo'=>'assets/images/product/c1.png'),

    );
  ?>
    <section id="products" class="section bg1 ">
        <div class="container">
            <div class="row pb-6">
                <div class="col-lg-12 text-center mb-3 mt-4 ">
                  <h3 class="title  ">  Our Products </h3> 
                </div>

                <div class="col-md-12">  
                  <br>
                </div>

                <?php 
                foreach ($data_product as $key => $value) {  ?>

                  <div class="col-lg-4 mb-4 " onclick="window.open(`<?= base_url().$value['link']; ?>`, '_blank').focus()"> 
                    <div class="card card-custom div-link" >
                      <div class="card-body row" >   
                            <div class="col-lg-3 text-center pb-2 "> 
                              <img style="max-height: 230px;" class="img-fluid" src="<?= base_url().$value['photo']; ?>"> 
                            </div>
                            <div class="col pl-3 d-flex justify-content-center">
                              <h3 class="title align-self-center  text-center "> <?= $value['product']; ?> </h3> 
                            
                            </div>
                            <br>
                            <br>
                        
                      </div>
                    </div>
                  </div> 
                  
                <?php } ?>
                
                
 
        </div> 
    </section>